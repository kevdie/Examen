package com.ucbcba;

import java.util.ArrayList;
import java.util.List;

public class Articulo implements FuncionGenerica {

    private String texto;
   // private int likes;
    private Usuario autor;
    private List<Usuario> likes;
    private List<Comentario> comentarios;

    public Articulo(){
        //this.likes = 0;
    }
    public Articulo(String texto, String nombre,String apellido){
        autor=new Usuario(nombre,apellido);
       // this.likes = 0;
        this.texto = texto;
        likes = new ArrayList<>();
        comentarios = new ArrayList<>();
    }

    public int numeroComentarios(){
        return this.comentarios.size();
    }

    public boolean isPalabra(String palabra){
        /*
        String caracteres  = "abcdefghijklmnopqrs";
        for(int i=0; i < palabra.length(); i++){
            if(caracteres.contains(String.valueOf(palabra.charAt(i)))){
                return true;
            }
        }
        return false;*/
        return palabra.matches("(a-zA-Z)");
    }

    public int numeroPalabras(){
        String[] arreglo = this.texto.split(" ");
        int contador = 0;
        for(String palabra : arreglo){
            if(isPalabra(palabra)){
                contador++;
            }
        }
        return contador;
    }


    public void like(Usuario autor)
    {
        this.likes.add(autor);
    }

    @Override
    public void disLike(String autor){
        int id=0,ida=0;
        for (Usuario user:likes)
        {
            if(user.getNombre().equals(autor))
            {
                likes.remove(user);
                id=-1;
            }
            else{
                id++;
            }
        }
        if (ida==0)
        {
            new UserNotFound();
        }


    }

    public int getLikes() {
        return likes.size();
    }

    //public void setLikes(int likes) {
       // this.likes = likes;
  //  }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public void anadirComentario(Comentario comentario) throws ComentarioVacioException, ComentarioLengthException {
        if(comentario.getTexto().equals("")){
           throw  new ComentarioVacioException();
        }
        if(comentario.getTexto().length() <= 50){
            throw  new ComentarioLengthException(comentario.getTexto().length());
        }
        this.comentarios.add(comentario);
    }

    public void borrarComentario(Integer id){
        this.comentarios.remove(id);
    }
    public void mostrar(){
        System.out.println(getTexto());
        System.out.println("Likes:"+likes.size());
        for(Usuario a : likes)
        {
            System.out.print(a.getNombre()+",");
        }
        System.out.println();
        System.out.println("Autor:"+autor.getNombre());
        System.out.println("Comentarios:");
       // int a=0;
        for(Comentario coment : comentarios)
        {
            //System.out.println("Texto comentario "+a);
            coment.mostrarComentario();
            //a++;
        }
    }
    public int numeroPalabrasComentarios(){
        int cont=0;
        for (Comentario coment : comentarios)
        {
            if(coment.getTexto().matches("(0-9)"))
            {
                cont++;
            }
        }
        return cont;
    }

    /*@Override
    public void disLike() {

    }*/
}
