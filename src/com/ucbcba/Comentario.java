package com.ucbcba;

import java.awt.*;

public class Comentario implements FuncionGenerica {
    private String texto;
    private int likes;
    private Usuario autor;

    public Comentario(){
        this.likes = 0;
    }
    public Comentario(String texto, String nombre,String apellido){
        this.autor=new Usuario(nombre,apellido);
        this.likes = 0;
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int numeroPalabras(){
        int cont=0;
        String aux=texto.substring(0,0);
        if (!aux.matches("(a-zA-Z)"))
        {
            cont++;
        }
        return cont;

    }

    public void like(){
        this.likes++;
    }
    public void mostrarComentario(){
        System.out.println(getTexto());
        System.out.println("Autor: "+autor.getNombre());
        System.out.println("Likes: "+getLikes());
    }
    @Override
    public void disLike(String autor){
        likes--;
    }

}
